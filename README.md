# Icinga2.Wizard
Icinga2 to automate config generation.

## What does it do?
The idea its to "make a bridge" between the classic Icinga2 configuration and the [Icinga Director](https://icinga.com/docs/director/latest/doc/01-Introduction/).

At work we don't want to migrate all our configuration to the Icinga Director (or at least, not yet), but sometimes people forget to add configurations for new hosts or services to icinga and things get left unchecked.

So, enter the Icinga2.Wizrd. With his shinny hat it will create all the configuration tools for you (or at least try!), so you can create some sort of automation (i.e: when a minion joins a saltstack master, create a host configuration with the grains)

The idea is to create a simple wizard AND a command line interface, kinda like
    
    i2wi.py --template generic-host-behind-fw --name dockerhost1 --etc.

It's a work in progress, buy hey, work.

## Initial setup

* Capture templates from icinga2

## Any ideas?
@ me at [@cachosagan](https://twitter.com/cachosagan) on Twitter. I speak Spanish and English